# Aide et ressources de ANA-ROD pour Synchrotron SOLEIL

[![](http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD/images/anarodtitle.gif)](http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD)

## Résumé
- ANA: Integration des rod F². ROD: fit du F² le long des rods et comparaison avec un modèle.
- Open source

## Sources
- Code source: http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD/download.php
- Documentation officielle: http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD/

## Navigation rapide
| Wiki SOLEIL | Fichiers téléchargeables | Page pan-data |
| ------ | ------ | ------ |
| [Tutoriel d'installation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ana-rod-documentation/-/wikis/Setup-guide) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ana-rod-documentation/-/tree/master/documents) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/113/rod) |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ana-rod-documentation/wikis/home) | | |

## Installation
- Systèmes d'exploitation supportés: Windows, Linux
- Installation: Facile (tout se passe bien)

## Format de données
- en entrée: texte
- en sortie: texte, PGPLOT
- sur un disque dur, Ruche locale
