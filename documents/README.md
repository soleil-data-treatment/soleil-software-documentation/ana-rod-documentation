| Document | Description |
| ------ | ------ |
| [rodmanual.pdf](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ana-rod-documentation/-/blob/master/documents/rodmanual.pdf) | User manual for ROD by Elias Vlieg - 11 September 2018 |
| [INSTALL.UNIX](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ana-rod-documentation/-/blob/master/documents/INSTALL.UNIX) | Official ANAROD installation manual - 2011 |
